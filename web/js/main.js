$(function () {
    $('#location').on('keyup', function (e) {
        var regexp = /[^a-zA-Z]/g;
        if ($(this).val().match(regexp)) {
            $(this).val($(this).val().replace(regexp, ''));
        }
    })
    $("#forecast_history").on('change', function(){
        $('#forecast').submit();
    });


    $('#forecast').on('submit', function (e) {
        e.preventDefault();
        var l = Ladda.create(document.querySelector( 'button' ));
        l.start();
        var $form = $(e.target);
        $.post($form.attr('action'), $form.serialize(), function (data) {
            $('#weather').find('li').remove();
            if (data.message === null) {
                $(data.forecast).each(function (i,jdata) {
                    var text = jQuery.map( jdata, function( n, i ) { return i + ': ' + n; });
                    $('<li/>').text(text.join( ", " ) ).appendTo('#weather')
                })
            } else {
                $('<li/>').text(data.message).appendTo('#weather')
            }
        }).always(function() { l.stop(); });;
    })
})

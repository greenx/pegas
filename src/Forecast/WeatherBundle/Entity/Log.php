<?php

namespace Forecast\WeatherBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 * Лог запросов к сервисам
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Log
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */

    protected $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="days", type="integer")
     */
    private $days;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255)
     */
    private $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var object
     *
     * @ORM\Column(name="data", type="object")
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="apiStatus", type="string", length=255, nullable=true)
     */
    private $apiStatus;


    public function __construct($obj = array())
    {
        $this->location = isset($obj['location']) ? $obj['location'] : Null;
        $this->days = isset($obj['days']) ? $obj['days'] : Null;
        $this->comments = isset($obj['comments']) ? $obj['comments'] : Null;
        $this->ip = isset($obj['ip']) ? $obj['ip'] : Null;
        $this->data = isset($obj['data']) ? $obj['data'] : Null;
        $this->id = isset($obj['history']) ? $obj['history'] : Null;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param integer $location
     * @return log
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set days
     *
     * @param integer $days
     * @return log
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return integer
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return log
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return log
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return apiStatus
     */
    public function getApiStatus()
    {
        return $this->apiStatus;
    }

    /**
     * @param apiStatus $apiStatus
     */
    public function setApiStatus($apiStatus)
    {
        $this->apiStatus = $apiStatus;
    }

    public function getHistory()
    {
        return implode(', ', [$this->createdAt->format('d/m/Y'), $this->location, $this->days, $this->comments]);
    }

    /**
     * Обновление даты
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}

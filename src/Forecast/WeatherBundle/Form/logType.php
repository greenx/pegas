<?php
namespace Forecast\WeatherBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Форма запроса погоды
 * Class logType
 * @package Forecast\WeatherBundle\Form
 */
class logType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('location', 'text', array('label' => 'Город'))
            ->add('days', 'choice', array(
                'label' => 'Дней',
                'choices' => array_combine(range(1, 14), range(1, 14)),
                'required' => true,
            ))
            ->add('comments', 'textarea', array('label' => 'Комментарий'))
            ->add('history', 'entity', array('class' => 'ForecastWeatherBundle:Log', 'property' => 'history', 'label' => "История", 'mapped' => false, 'empty_value' => 'Выбрать', 'required' => false));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Forecast\WeatherBundle\Entity\Log'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'forecast';
    }
}

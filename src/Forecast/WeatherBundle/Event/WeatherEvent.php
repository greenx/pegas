<?php namespace Forecast\WeatherBundle\Event;
use Symfony\Component\EventDispatcher\Event;
use Forecast\WeatherBundle\Entity\Log;

/**
 * Class WeatherEvent
 * @package Forecast\WeatherBundle\Event
 */
class WeatherEvent extends Event
{
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    public function getLog()
    {
        return $this->log;
    }



}
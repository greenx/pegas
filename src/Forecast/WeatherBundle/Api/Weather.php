<?php
namespace Forecast\WeatherBundle\Api;

/**
 * прогноз на день
 * Class WeatherItem
 * @package Forecast\WeatherBundle\libs
 */
class WeatherItem
{
    // День недели
    public $day = null;

    // Температура дневная, максимальная, минимальная
    public $temperature = null;
    public $min = null;
    public $max = null;


}

/**
 * Прогноз
 * Class Weather
 * @package Forecast\WeatherBundle\libs
 */
abstract class Weather
{
    //protected $today = null;
    public $forecast = null;

    abstract public function __construct($city_code, $days);

    /**
     * @return array of WeatherConditions forecasts
     */
    public function forecast()
    {
        return $this->forecast;
    }
}
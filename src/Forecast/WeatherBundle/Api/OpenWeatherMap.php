<?php
namespace Forecast\WeatherBundle\Api;
use GuzzleHttp;

/**
 * API погодного сервиса OpenWeatherMap
 * http://openweathermap.org/forecast
 * Class OpenWeatherMap
 * @package Forecast\WeatherBundle\Api
 */
class OpenWeatherMap extends Weather
{
    /**
     * @param $city_code
     * @param $days
     * @throws Exception
     */
    public function __construct($city_code, $days)
    {

        $client = new GuzzleHttp\Client(['base_url' => 'http://api.openweathermap.org/data/2.5/']);

        $response = $client->get('forecast/daily', ['query' => [
            'q' => $city_code,
            'cnt' => $days,
            'mode' => 'json',
            'units' => 'metric']
        ]);

        $data = json_decode($response->getBody());
        if($data->cod == 404){
            throw new \Exception("город не найден");
        } elseif($data->cod !=200){
            throw new \Exception($data->message);
        }

        $this->forecast = array();
        foreach ($data->list as $tforecast) {
            $forecast = new WeatherItem();
            $forecast->day = (isset($tforecast->dt) ? (string)date('d.m.Y',$tforecast->dt) : null);
            $forecast->temperature = (isset($tforecast->temp->day) ? (string)$tforecast->temp->day : null);
            $forecast->min = (isset($tforecast->temp->min) ? (string)$tforecast->temp->min : null);
            $forecast->max = (isset($tforecast->temp->max) ? (string)$tforecast->temp->max : null);
            $this->forecast[] = $forecast;
        }
    }


}

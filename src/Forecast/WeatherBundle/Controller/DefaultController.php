<?php

namespace Forecast\WeatherBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Forecast\WeatherBundle\Event\WeatherEvent;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Forecast\WeatherBundle\Form\logType;
use Forecast\WeatherBundle\Entity\Log;

/**
 * Контроллер: получение прогноза погоды
 * Class DefaultController
 * @package Forecast\WeatherBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Прогноз погоды: Форма запроса
     *
     * @Route("/", name="log.form")
     * @Method("GET")
     * @Template("ForecastWeatherBundle:Default:form.html.twig")
     */
    public function newAction()
    {
        $entity = new Log();
        $form = $this->createForm(new logType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Прогноз погоды: Обра формы
     * @Route("/", name="log.save")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $data = array_merge(
            $request->get('forecast'),
            ['ip' => $request->getClientIp()]
        );
        $dispatcher = $this->get('event_dispatcher');
        $response = $dispatcher->dispatch('weather.event', new WeatherEvent(new Log($data)));

        $forecast = $this->getDoctrine()->getRepository('ForecastWeatherBundle:Log')->find($response->getLog()->getId());
        $response = array_merge((array)$forecast->getData(), ['message' => $forecast->getApiStatus()]);
        return new JsonResponse($response);
    }
}
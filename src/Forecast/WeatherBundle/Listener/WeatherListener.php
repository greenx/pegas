<?php
namespace Forecast\WeatherBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use Forecast\WeatherBundle\Api\OpenWeatherMap;
use Doctrine\ORM\EntityManager;

/**
 * Обработка запроса погоды
 * Class WeatherListener
 * @package Forecast\WeatherBundle\Listener
 */
class WeatherListener
{
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Получение данных о погоде
     * @param Event $event
     * @return mixed
     */
    public function onWeatherEvent(Event $event)
    {
        $log = $event->getLog();

        if ($log->getId()) return $log;

        $tip = substr(strrchr($log->getIp(), '.'), 1);
        if (fmod($tip, 2) == 0) {
            $forecast = array();
            try {
                $forecast = new OpenWeatherMap($log->getLocation(), $log->getDays());
            } catch (\Exception $e) {
                $log->setApiStatus($e->getMessage());
            }

            $log->setData($forecast);
            $this->em->persist($log);
            $this->em->flush();
            return $log;
        } else {
            echo $tip . "не четный, другой сервис";
        }
    }
}